<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class About_UsController extends Controller
{
    public function index()
    {
        return  DB::table('about_us')->get();
    }

    public function show($id)
    {
        return  DB::table('about_us')->find($id);
    }

    public function store(Request $request)
    {
        $about_us =  DB::table('about_us')->create($request->all());
        return response()->json($about_us, 201);;
    }

    public function update(Request $request, $id)
    {
        $about_us =  DB::table('about_us')->findOrFail($id);
        $about_us->update($request->all());

        return response()->json($about_us, 200);;
    }

    public function delete(Request $request, $id)
    {
        $about_us =  DB::table('about_us')->findOrFail($id);
        $about_us->delete();

        return response()->json(null, 204);
    }
}