<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Items;

class ItemsController extends Controller
{
    public function index()
    {
        return Items::all();
    }

    public function show($id)
    {
        return Items::find($id);
    }

    public function store(Request $request)
    {
        $item = Items::create($request->all());
        return response()->json($item, 201);;
    }

    public function update(Request $request, $id)
    {
        $item = Items::findOrFail($id);
        $item->update($request->all());

        return response()->json($item, 200);;
    }

    public function delete(Request $request, $id)
    {
        $item = Items::findOrFail($id);
        $item->delete();

        return response()->json(null, 204);
    }
}