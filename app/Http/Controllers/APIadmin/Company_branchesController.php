<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class Company_branchesController extends Controller
{
    public function index()
    {
        return  DB::table('company_branches')->get();
    }

    public function show($id)
    {
        return  DB::table('company_branches')->find($id);
    }

    public function store(Request $request)
    {
        $company_branches =  DB::table('company_branches')->create($request->all());
        return response()->json($company_branches, 201);;
    }

    public function update(Request $request, $id)
    {
        $company_branches =  DB::table('company_branches')->findOrFail($id);
        $company_branches->update($request->all());

        return response()->json($company_branches, 200);;
    }

    public function delete(Request $request, $id)
    {
        $company_branches =  DB::table('company_branches')->findOrFail($id);
        $company_branches->delete();

        return response()->json(null, 204);
    }
}
