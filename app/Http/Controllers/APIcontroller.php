<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Product;
use App\Item;

class APIcontroller extends Controller
{
    public function about_us(Request $request)
    {
        // $request['lang'] = en OR ar
        if (isset($request['lang'])) 
        {
        	if ($request['lang'] == 'en') 
        	{
        		$about_us = DB::table('about_us')->value('about_en');

        		return $about_us;
        	}else{
        		$about_us = DB::table('about_us')->value('about_ar');

        		return $about_us;
        	}
        } 
        // return description_en or description_ar from about_us table 

        $about_us = DB::table('about_us')->get(['about_ar']);

        return $about_us;
    }

    public function company_branches (Request $request)
    {
    	if (isset($request['lang'])) 
    	{
    		if ($request['lang'] == 'en') 
    		{
    			$company_address = DB::table('company_branches')->get(['address_en']);
    			$company_phone1 = DB::table('company_branches')->get(['phone_one']);
    			$company_phone2 = DB::table('company_branches')->get(['phone_two']);

    			return array($company_branches, $company_phone1, $company_phone2);
    		}else{
    			$company_address = DB::table('company_branches')->get(['address_ar']);
    			$company_phone1 = DB::table('company_branches')->get(['phone_one']);
    			$company_phone2 = DB::table('company_branches')->get(['phone_two']);

    			return array($company_branches, $company_phone1, $company_phone2);
    		}
    	}

    	$company_address = DB::table('company_branches')->get(['address_ar']);
    	$company_phone1 = DB::table('company_branches')->get(['phone_one']);
    	$company_phone2 = DB::table('company_branches')->get(['phone_two']);

    	return array($company_branches, $company_phone1, $company_phone2);
    }

    public function get_all_categories(Request $request)
    {
        if (isset($request['lang'])) 
    	{
    		if ($request['lang'] == 'en') 
    		{
    			$category_title = DB::table('categories')->get(['title_en']);

    			return $category_title;
    		}else{
    			$category_title = DB::table('categories')->get(['title_ar']);

    			return $category_title;
    		}
    	}

    	$category_title = DB::table('categories')->get(['title_ar']);

    	return $category_title;
    }

    public function get_products_by_category(Request $request, $category_id)
    { 
        if (isset($request['lang'])) 
    	{
    		if ($request['lang'] == 'en') 
    		{
    			$title_en = Product::where('category_id', $category_id)->value('title_en');
    			$describtion_en = Product::where('category_id', $category_id)->value('describtion_en');
    			$img_url = Product::where('category_id', $category_id)->value('img_url');
    			
    			return array($title_en, $describtion_en, $img_url);
    		}else{
    			$title_ar = Product::where('category_id', $category_id)->value('title_ar');
    			$describtion_ar = Product::where('category_id', $category_id)->value('describtion_ar');
    			$img_url = Product::where('category_id', $category_id)->value('img_url');

    			return array($title_ar, $describtion_ar, $img_url);
    		}
    	}

    	$title_ar = Product::where('category_id', $category_id)->value('title_ar');
    	$describtion_ar = Product::where('category_id', $category_id)->value('describtion_ar');
    	$img_url = Product::where('category_id', $category_id)->value('img_url');

    	return array($title_ar, $describtion_ar, $img_url);
    }

    public function get_items_by_product (Request $request, $product_id)
    {
        if (isset($request['lang'])) 
        {
            if ($request['lang'] == 'en') 
            {
                $title_en = Item::where('product_id', $product_id)->value('title_en');
                $describtion_en = Item::where('product_id', $product_id)->value('describtion_en');
                $image = Item::where('product_id', $product_id)->valeu('img_url');
                $price = Item::where('product_id', $product_id)->value('price'); 

                return array($title_en, $describtion_en, $image, $price);              
            }else{
                $title_ar = Item::where('product_id', $product_id)->value('title_ar');
                $describtion_ar = Item::where('product_id', $product_id)->value('describtion_ar');
                $image = Item::where('product_id', $product_id)->value('img_url');
                $price = Item::where('product_id', $product_id)->value('price');

                return array($title_ar, $description_ar, $image, $price);
            }
        }

        $title_ar = Item::where('product_id', $product_id)->value('title_ar');
        $describtion_ar = Item::where('product_id', $product_id)->value('describtion_ar');
        $image = Item::where('product_id', $product_id)->value('img_url');
        $price = Item::where('product_id', $product_id)->value('price');

        return array($title_ar, $description_ar, $image, $price);

    }

    public function get_all_products (Request $request)
    {
        if (isset($request['lang'])) 
        {
            if ($request['lang'] == 'en') 
            {
                $title_en = Product::get(['title_en']);
                $describtion_en = Product::get(['describtion_en']);
                $img_url = Product::get(['img_url']);
                
                return array($title_en, $describtion_en, $img_url);
            }else{
                $title_ar = Product::get(['title_ar']);
                $describtion_ar = Product::get(['describtion_ar']);
                $img_url = Product::get(['img_url']);

                return array($title_ar, $describtion_ar, $img_url);
            }
        }

         $title_ar = Product::get(['title_ar']);
         $describtion_ar = Product::get(['describtion_ar']);
         $img_url = Product::get(['img_url']);

         return array($title_ar, $describtion_ar, $img_url);
    }

    public function get_all_items(Request, $request)
    {
        if (isset($request['lang'])) 
        {
            if ($request['lang'] == 'en') 
            {
                $title_en = Item::get(['title_en']);
                $describtion_en = Item::get(['describtion_en']);
                $image = Item::get(['img_url']);
                $price = Item::get(['price']); 

                return array($title_en, $describtion_en, $image, $price);              
            }else{
                $title_ar = Item::get(['title_ar']);
                $describtion_ar = Item::get(['describtion_ar']);
                $image = Item::get(['img_url']);
                $price = Item::get(['price']);

                return array($title_ar, $description_ar, $image, $price);
            }
        }

        $title_ar = Item::get(['title_ar']);
        $describtion_ar = Item::get(['describtion_ar']);
        $image = Item::get(['img_url']);
        $price = Item::get(['price']);

        return array($title_ar, $description_ar, $image, $price);
    }
}
