<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('about_us','APIController@about_us');
Route::get('/company_branches', 'APIController@company_branches');
Route::get('/categories', 'APIController@get_all_categories');
Route::get('/get_products_by_category', 'APIController@get_products_by_category');
Route::get('/get_products_by_category', 'APIController@get_items_by_product');
Route::get('/get_all_products'. 'APIController@get_all_products');
Route::get('/get_all_items', 'APIController@get_all_items');

//Admin Controls - categories
Route::get('categories', 'APIadmin/CategoriesController@index');
Route::get('categories/{id}', 'APIadmin/CategoriesController@show');
Route::post('categories', 'APIadmin/CategoriesController@store');
Route::put('categories/{id}', 'APIadmin/CategoriesController@update');
Route::delete('categories/{id}', 'APIadmin/CategoriesController@delete');

//Admin Controls - products
Route::get('products', 'APIadmin/ProductsController@index');
Route::get('products/{id}', 'APIadmin/ProductsController@show');
Route::post('products', 'APIadmin/ProductsController@store');
Route::put('products/{id}', 'APIadmin/ProductsController@update');
Route::delete('products/{id}', 'APIadmin/ProductsController@delete');

//Admin Controls - items
Route::get('items', 'APIadmin/ItemsController@index');
Route::get('items/{id}', 'APIadmin/ItemsController@show');
Route::post('items', 'APIadmin/ItemsController@store');
Route::put('items/{id}', 'APIadmin/ItemsController@update');
Route::delete('items/{id}', 'APIadmin/ItemsController@delete');

//Admin Controls - Company Branches
Route::get('company_branches', 'APIadmin/Company_branchesController@index');
Route::get('company_branches/{id}', 'APIadmin/Company_branchesController@show');
Route::post('company_branches', 'APIadmin/Company_branchesController@store');
Route::put('company_branches/{id}', 'APIadmin/Company_branchesController@update');
Route::delete('company_branches/{id}', 'APIadmin/Company_branchesController@delete');

//Admin Controls - About us
Route::get('about_us', 'APIadmin/About_UsController@index');
Route::get('about_us/{id}', 'APIadmin/About_UsController@show');
Route::post('about_us', 'APIadmin/About_UsController@store');
Route::put('about_us/{id}', 'APIadmin/About_UsController@update');
Route::delete('about_us/{id}', 'APIadmin/About_UsController@delete');



